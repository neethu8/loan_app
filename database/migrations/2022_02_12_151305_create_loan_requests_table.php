<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLoanRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loan_requests', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id');
            $table->float('amount_required');
            $table->integer('loan_term');
            $table->float('remaining_amount')->nullable();
            $table->integer('remaining_terms')->nullable();
            $table->dateTime('last_repayment_date')->nullable();
            $table->string('loan_status')->comment("statuses: submitted, accepted, rejected, completed")->default("submitted");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loan_requests');
    }
}
