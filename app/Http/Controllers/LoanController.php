<?php

namespace App\Http\Controllers;

use App\Models\LoanRepayment;
use App\Models\LoanRequest;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class LoanController extends Controller
{

    /**
     * Create a new LoanController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /*
    * @int loan_term is the no.of terms the loan should be repayed
    * @flat amount_required is the loan amount required by the customer
    */
    public function requestLoan(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'amount_required' => 'required|integer',
            'loan_term' => 'required|integer'
        ]);
        if ($validator->fails()) {
            return Response::json([
                'status' => false,
                'error' => $validator->errors()->all(),
            ], 400);
        } else {
            $user_id = auth()->user()->id;

            $loan_request = new LoanRequest();
            $loan_request->amount_required =   $loan_request->remaining_amount = $request->amount_required;
            $loan_request->loan_term = $loan_request->remaining_terms =  $request->loan_term;
            $loan_request->user_id = $user_id;
            $loan_request->save();

            return response()->json([
                'status' => true,
                'message' => "Successfully requested loan.",
            ]);
        }
    }

    /*
    * loan can be repayed in any date,
    * the repayed amout will be deducted from the total loan amount each time
    * considering there is no interest for the loan.
    * 
    */
    public function repayLoan(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'repay_amount' => 'required|integer',
            'loan_id' => 'required'
        ]);
        if ($validator->fails()) {
            return Response::json([
                'status' => false,
                'error' => $validator->errors()->all(),
            ], 400);
        } else {

            $loan = LoanRequest::where('id', '=', $request->loan_id)->first();

            if ($loan == null) {
                return response()->json(['error' => "Something went wrong.. Invalid loan id passed.. :("], 401);
            } else {

                switch ($loan->loan_status) {
                    case "submitted":
                        return response()->json(['error' => "Your loan has not approved yet. Please be patient.. We will update you soon.. :)"], 401);
                        break;
                    case "rejected":
                        return response()->json(['error' => "Your loan was rejected.."], 401);
                        break;
                    case "completed":
                        return response()->json(['error' => "Your loan repaymet was completed.. Thank you :)"], 401);
                        break;
                }
                $term_amount = ceil($loan->amount_required / $loan->loan_term);
                if ($request->repay_amount != $term_amount) {
                    return response()->json(['error' => "Your weekly repay amount is  $term_amount Please fill repay amount field with $term_amount to continue.."], 401);
                }

                $repayment = new LoanRepayment();
                $repayment->user_id = $loan->user_id;
                $repayment->repay_amount = $request->repay_amount;
                $repayment->status = 1;
                $repayment->save();

                $loan->remaining_amount = $loan->remaining_amount - $request->repay_amount;
                $loan->remaining_terms--;
                $loan->last_repayment_date = Carbon::now();
                $loan->save();

                if ($loan->remaining_terms == 0) {
                    $loan->loan_status = "completed";
                    $loan->remaining_amount = 0;
                    $loan->save();
                }
                return response()->json([
                    'status' => true,
                    'message' => "Success",
                ]);
            }
        }
    }
}
