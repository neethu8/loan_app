<?php

namespace App\Http\Controllers;

use App\Models\LoanRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /*
    * Get all loan requests
    */
    public function getLoanRequests()
    {
        $loan_requests = LoanRequest::all();

        return response()->json([
            'status' => true,
            'loans' => $loan_requests,
            'message' => "Successfully fetched loans.",
        ]);
    }

    /*
     * Change loan status for approval/ rejection by admin
     *  @status = boolean, 1 means accepeted, 0 means rejected.
     */
    public function changeLoanStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'loan_id' => 'required',
            'status' => 'required'
        ]);
        if ($validator->fails()) {
            return Response::json([
                'status' => false,
                'error' => $validator->errors()->all(),
            ], 400);
        } else {

            $loan_request = LoanRequest::where('id', '=', $request->loan_id)->first();

            if ($loan_request != null) {
                $status = $request->status == 1  ? "accepted" : "rejected";

                $loan_request->loan_status = $status;
                $loan_request->save();
                //send mail/message to the customer

                return response()->json([
                    'status' => true,
                    'message' => "Loan status changed successfully.",
                ]);
            } else {
                return response()->json(['error' => "Something went wrong.. Invalid loan id passed.. :("], 401);
            }
        }
    }
}
