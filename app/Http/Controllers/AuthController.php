<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Mail;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login', 'signup']]);
    }

    /*
    * Signup for customer
    */
    public function signup(Request $request)
    {
        $messages = array(
            'name.required' => 'Name is required',
            'name.string' => 'Name must be a string',
            'name.max' => 'Maximum characters allowed in name is 100',
            'email.required' => 'Email is required',
            'email.unique' => 'This email is already in use, please give another one.',
        );

        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email',
            'password' => 'required|min:8|confirmed'
        ], $messages);
        if ($validator->fails()) {
            return Response::json([
                'status' => false,
                'error' => $validator->errors()->all(),
            ], 400);
        } else {
            $user = new User();
            $user->email = $request->get('email');
            $user->name = $request->get('name');
            $user->password = Hash::make($request->get('password'));

            $user->save();
            return response()->json([
                'status' => true,
                'message' => "Successfully signed up.. :)",
            ]);
        }
    }

    /*
    * Login function for both admin and user.
    */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required',
            'password' => 'required'
        ]);
        if ($validator->fails()) {
            return Response::json([
                'status' => false,
                'error' => $validator->errors()->all(),
            ], 400);
        } else {
            $credentials = request(['email', 'password']);

            if (!$token = auth()->attempt($credentials)) {
                return response()->json(['error' => "Unauthorized"], 401);
            }

            return $this->respondWithToken($token);
        }
    }

    /*
    * Respond with jwt token for authentication
    */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
}
