-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 13, 2022 at 05:51 PM
-- Server version: 10.4.18-MariaDB
-- PHP Version: 8.0.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `loan_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `loan_repayments`
--

CREATE TABLE `loan_repayments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `repay_amount` double(8,2) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `loan_repayments`
--

INSERT INTO `loan_repayments` (`id`, `user_id`, `repay_amount`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 3333.00, 1, '2022-02-13 10:05:49', '2022-02-13 10:05:49'),
(2, 1, 3333.00, 1, '2022-02-13 10:06:06', '2022-02-13 10:06:06'),
(3, 1, 3333.00, 1, '2022-02-13 10:06:22', '2022-02-13 10:06:22'),
(4, 1, 3333.00, 1, '2022-02-13 10:06:34', '2022-02-13 10:06:34'),
(5, 1, 3333.00, 1, '2022-02-13 10:06:34', '2022-02-13 10:06:34'),
(6, 1, 3333.00, 1, '2022-02-13 10:06:35', '2022-02-13 10:06:35'),
(7, 1, 3333.00, 1, '2022-02-13 10:06:41', '2022-02-13 10:06:41'),
(8, 1, 3333.00, 1, '2022-02-13 10:06:41', '2022-02-13 10:06:41'),
(9, 1, 3333.00, 1, '2022-02-13 10:06:42', '2022-02-13 10:06:42'),
(10, 1, 3333.00, 1, '2022-02-13 10:06:48', '2022-02-13 10:06:48'),
(11, 1, 3333.00, 1, '2022-02-13 10:06:49', '2022-02-13 10:06:49'),
(12, 1, 3333.00, 1, '2022-02-13 10:06:54', '2022-02-13 10:06:54'),
(13, 1, 3333.00, 1, '2022-02-13 10:07:01', '2022-02-13 10:07:01'),
(14, 1, 3334.00, 1, '2022-02-13 10:09:04', '2022-02-13 10:09:04'),
(15, 1, 100.00, 1, '2022-02-13 10:10:26', '2022-02-13 10:10:26'),
(16, 1, 100.00, 1, '2022-02-13 10:10:41', '2022-02-13 10:10:41'),
(17, 1, 100.00, 1, '2022-02-13 10:10:42', '2022-02-13 10:10:42'),
(18, 1, 100.00, 1, '2022-02-13 10:10:42', '2022-02-13 10:10:42'),
(19, 1, 100.00, 1, '2022-02-13 10:10:43', '2022-02-13 10:10:43'),
(20, 1, 100.00, 1, '2022-02-13 10:10:48', '2022-02-13 10:10:48'),
(21, 1, 100.00, 1, '2022-02-13 10:10:49', '2022-02-13 10:10:49'),
(22, 1, 100.00, 1, '2022-02-13 10:10:50', '2022-02-13 10:10:50'),
(23, 1, 100.00, 1, '2022-02-13 10:10:56', '2022-02-13 10:10:56'),
(24, 1, 100.00, 1, '2022-02-13 10:11:01', '2022-02-13 10:11:01');

-- --------------------------------------------------------

--
-- Table structure for table `loan_requests`
--

CREATE TABLE `loan_requests` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `amount_required` double(8,2) NOT NULL,
  `loan_term` int(11) NOT NULL,
  `remaining_amount` double(8,2) DEFAULT NULL,
  `remaining_terms` int(11) DEFAULT NULL,
  `last_repayment_date` datetime DEFAULT NULL,
  `loan_status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'submitted' COMMENT 'statuses: submitted, accepted, rejected, completed',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `loan_requests`
--

INSERT INTO `loan_requests` (`id`, `user_id`, `amount_required`, `loan_term`, `remaining_amount`, `remaining_terms`, `last_repayment_date`, `loan_status`, `created_at`, `updated_at`) VALUES
(1, 1, 40000.00, 12, 0.00, 0, '2022-02-13 15:39:04', 'completed', '2022-02-13 10:05:19', '2022-02-13 10:09:04'),
(2, 1, 1000.00, 10, 0.00, 0, '2022-02-13 15:41:01', 'completed', '2022-02-13 10:09:43', '2022-02-13 10:11:01');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(16, '2022_02_12_155215_create_admins_table', 2),
(17, '2022_02_12_183435_remove_token_columns_to_users_table', 3),
(18, '2022_02_12_183543_remove_token_columns_from_admins_table', 4),
(67, '2014_10_12_000000_create_users_table', 5),
(68, '2014_10_12_100000_create_password_resets_table', 5),
(69, '2019_08_19_000000_create_failed_jobs_table', 5),
(70, '2019_12_14_000001_create_personal_access_tokens_table', 5),
(71, '2022_02_12_151305_create_loan_requests_table', 5),
(72, '2022_02_13_144921_create_loan_repayments_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_admin` tinyint(1) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `is_admin`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', NULL, '$2y$10$JgiB1CpS3/DzwXygX6bDruucuN18bmeWx/HECAeTUdm0NbGKxKEhK', 1, NULL, '2022-02-13 10:04:57', '2022-02-13 10:04:57'),
(2, 'Test user', 'testuser1@gmail.com', NULL, '$2y$10$utiPrBGP9IGd6u3AWO0upecqOIz/UYjlKQKzt.0eD3YNYLGBkgg8u', NULL, NULL, '2022-02-13 10:05:01', '2022-02-13 10:05:01');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `loan_repayments`
--
ALTER TABLE `loan_repayments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `loan_requests`
--
ALTER TABLE `loan_requests`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `loan_repayments`
--
ALTER TABLE `loan_repayments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT for table `loan_requests`
--
ALTER TABLE `loan_requests`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=73;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
