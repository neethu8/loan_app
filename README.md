STEPS TO RUN THE PROJECT
------------------------
1. Run composer install to install vendor files
2. Rename .env.example with .env
3. Exported database is in root folder(loan_app.sql). Either import it or use database      migration.
4. Exported postman collection is in root folder(loan_app.postman_collection.json)

IMPORTANT POINTS
------------------------------------------------------------
1. isAdmin middleware user to differentiate between admin and user login
2. Assumed there is no interest for the loan.
3. The repay amount is a fixed amount, ie calculated by amount required divided by total loan terms.
4. The loan terms should be enter in weeks.
5. Each loan will be initially in status "submitted", then admin can change it to either accepted or rejected. Then after successfull competion of loan repayment the status will be changed to "completed"

SIGNUP(for customer)
------------------------------------------------------------
Customers need to signup for applying loans.
Sign up can be done with entering name, email, password and confirm password.
After successful signup customers can login with the corresponding email and password.

api name: signup,
example api: http://127.0.0.1:8000/api/loan-app/signup,
method: post,

LOGIN(for customer and admin)
------------------------------------------------------------
Implemeted login for customer and admin. Use DatabaseSeeder.php seeder file to insert admin data (already there is one in the expoerted table. 
ie,
email: admin@gmail.com
password: 123456
)
JWT token used for authentication. use authorization header with value 'bearer <token>' for all api's after login. JWT token will be returned in login result with name access_token.

api name: login,
example api: http://127.0.0.1:8000/api/loan-app/login,
method: post

LOAN REQUEST(customer api)
------------------------------------------------------------
Customer can  request loan by filling amount required and loan term(in weeks) fields. 
The api call should be filled with header Authorization with token generated in login.

api name: request-loan,
example api: http://127.0.0.1:8000/api/loan-app/request-loan,
method: post

REPAY LOAN(customer api)
------------------------------------------------------------
loan repayment can be done with repay amount and loan id.
the repay is calculated by dividing amount required with loan term.
If the amount is not correct an error message will be generated.
After successful completion of loan repayment the loan status will be changed to "completed".

Assuming there is no interest for the loan.

The api call should be filled with header Authorization with token generated in login.

api name: repay-loan,
example api: http://127.0.0.1:8000/api/loan-app/repay-loan,
method: post

LOAN REQUESTS(admin api)
------------------------------------------------------------
The admin can view all the loan requests by calling this api.
The api call should be filled with header Authorization with token generated in admin login.

api name: loan-requests,
example api: http://127.0.0.1:8000/api/loan-app/loan-requests,
method: get

LOAN STATUS(admin api)
------------------------------------------------------------
The admin can change the status of loan request through this api.
The loan request will be in the status "submitted" initially.
Then the logged in admin can change the status of each loan request to either "accepted" or "rejected", use status 1 for "accepted" and 0 for "rejected".

The api call should be filled with header Authorization with token generated in admin login.

api name: loan-status,
example api: http://127.0.0.1:8000/api/loan-app/loan-status,
method: post
